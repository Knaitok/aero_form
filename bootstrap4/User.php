<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.05.19
 * Time: 22:25
 */

class User
{
#region labels
private $name;
private $secondName;
private $thirdName;
private $tel;
private $email;
private $birthday;
private $message;
private $comment;
#endregion

public function __construct($name, $secondName, $thirdName, $tel, $email, $birthday)
    {
      $this->name = $name;
      $this->secondName = $secondName;
      $this->thirdName = $thirdName;
      $this->tel = $tel;
      $this->email = $email;
      $this->birthday = $birthday;
      $this->startCheck();
    }
    public function startCheck() {
    $this->check_fio($this->name);
    $this->check_fio($this->secondName);
    $this->check_fio($this->thirdName);
    $this->valid_number($this->tel);
    $this->valid_email($this->email);
    $this->happy_day($this->birthday);
    }

    public function printMessage() {
    return $this->message;
    }
    public function check_fio($fio)
    {
        if (!empty($fio)) {
            $fio = trim($fio);
            if (iconv_strlen($fio) < 20) {
                if (preg_match("/^[а-яА-Яa-zA-Z]+$/iu", $fio)) {
                    return $fio;
                } else {

                    $this->message .= "Значение ФИО: " . $fio . "указано неверно;<br>";
                }
            }
        } else {

            $this->message .= "Значения ФИО:" . $fio . "не заполнены;<br>";
        }
    }
    public function valid_number($number)
    {
        if (!empty($number)) {
            $number = trim($number);
            if (iconv_strlen($number) < 11) {
                if (!ctype_digit($number)) {
                    $this->message .= "Номер телефона введен неверно;<br>";
                } else {
                    $number = "8" . $number;
                }
            } else {
                $this->message .= "Номер телефона введен неверно";
            }
        } else {
            $this->message .= "Введите номер телефона;<br>";
        }
    }
    public function valid_email($mail)
    {
        if (!empty($mail)) {
            if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                $this->message  .= "E-mail адрес .$mail. указан неверно;<br>";
            }
        } else {
            $this->message  .= "Введите E-mail;<br>";
        }
    }
    public function happy_day($happy_birthday)
    {
        if (!empty($happy_birthday)) {
            $max_data = date_create(date("Y-m-d"));
            $min_data = date_modify(date_create(date("Y-m-d")), '-100 year');
            $happy_birthday1 = date_create($happy_birthday);
            if ($min_data > $happy_birthday1 && $max_data < $happy_birthday1) {
                $this->message .= "Дата указана неверно;<br>";
            }
        } else {
            $this->message .= "Введите дату;<br>";
        }
    }
    public function ModifyComment($comment) {
    $this->comment = htmlspecialchars($comment);
    }
    #region getters
    public function get_name() {
    return $this->name;
    }
    public function get_SecondName() {
    return $this->secondName;
    }
    public function get_ThirdName(){
        return $this->thirdName;
    }
    public function get_tel() {
        return $this->tel;
    }
    public function get_email() {
        return $this->email;
    }
    public function get_birthday() {
        return $this->birthday;
    }
    public function get_comment() {
        return $this->comment;
    }
    #endregion

}