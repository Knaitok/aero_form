<?php
require_once 'User.php';
require_once 'Connection.php';
require_once 'Captcha.php';

//Условие рекапчи
if (isset($_POST['g-recaptcha-response'])) {
    $captcha = new Captcha();
    if ($captcha->getResponse()->success) {
//начало валидации
    $user = new User($_POST['name'],
                     $_POST['last_name'],
                     $_POST['first_name'],
                     $_POST['number'],
                     $_POST['Email'],
                     $_POST['data']);
        $connection = new Connection();
        $openConn = $connection->createConnectionToDB();

        $comment = mysqli_real_escape_string($openConn, $user->get_comment());
        $user->modifyComment($comment);
        if ($user->printMessage() == '') {
            $connection->applyQuery(
                $user->get_name(),
                $user->get_secondName(),
                $user->get_thirdName(),
                $user->get_tel(),
                $user->get_email(),
                $user->get_birthday(),
                $user->get_comment()
            );

        } else {
            $user->printMessage();
        }
        $openConn->close();
//Конец валидации
    } else {
        exit('Извините но похоже вы робот \(0_0)/');
    }
} else {
    exit('Вы не прошли валидацию reCaptcha');
}
