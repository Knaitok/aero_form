<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 15.05.19
 * Time: 23:27
 */

class Captcha
{
    const SECRET = "6Lfw55gUAAAAAGwDXW1VUsmRhOkZBEy_ikTnTejP";
    private $recaptchaResponse;
    private $response;
    private $remote_ip;
    private $query;

    public function __construct()
    {
        $this->recaptchaResponse = $_POST['g-recaptcha-response'];
        $this->remote_ip = $_SERVER['REMOTE_ADDR'];
        $this->query = 'https://www.google.com/recaptcha/api/siteverify?secret=' .self::SECRET.'&response='.$this->recaptchaResponse.'&remoteip='.$this->remote_ip;
        $this->response = json_decode(file_get_contents($this->query));
    }

    public function getResponse()
    {
        return $this->response;
    }

}